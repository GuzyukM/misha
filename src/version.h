#ifndef VERSION_H
#define VERSION_H

#define MAJORVER 2
#define MINORVER 1
#define REVISION 12
#define MINOR_REVISION 0
#define BETA 0
#define BUILD_TIME "15.02.2012"

#endif // VERSION_H
